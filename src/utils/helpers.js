const validateUrl = (url) => {
  const regexUrl = new RegExp(/(^http[s]?:\/{2})|(^www)|(^\/{1,2})/gim);
  return regexUrl.test(url);
};

export const helpers = {
  validateUrl,
};
