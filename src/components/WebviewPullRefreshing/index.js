import React, {useRef, useState} from 'react';
import {WebView} from 'react-native-webview';
import {RefreshControl, StyleSheet, ScrollView, View, Text} from 'react-native';
import {COLORS} from '../../constants/colors';
import {helpers} from '../../utils/helpers';

// prettier-ignore
const INJECTED_JS = `
  window.onscroll = function() {
    window.ReactNativeWebView.postMessage(
      JSON.stringify({
        scrollTop: document.documentElement.scrollTop || document.body.scrollTop
      }),     
    )
  }
`

const SCROLLVIEW_CONTAINER = {
  flex: 1,
  height: '100%%',
};

const WEBVIEW = (height) => ({
  width: '100%%',
  height,
});

export const WebviewPullRefreshing = ({
  url = 'https://www.24h.com.vn/',
  ...props
}) => {
  const webViewRef = useRef(null);
  const [isPullToRefreshEnabled, setIsPullToRefreshEnabled] = useState(false);
  const [scrollViewHeight, setScrollViewHeight] = useState(0);
  state = {
    isPullToRefreshEnabled: false,
    scrollViewHeight: 0,
  };

  const onRefresh = () => webViewRef.current.reload();

  const onWebViewMessage = (e) => {
    const {data} = e.nativeEvent;

    try {
      const {scrollTop} = JSON.parse(data);
      setIsPullToRefreshEnabled(scrollTop === 0);
    } catch (error) {
      console.log('----onWebViewMessage---error----', error);
    }
  };

  return (
    <>
      {helpers.validateUrl(url) ? (
        <ScrollView
          style={SCROLLVIEW_CONTAINER}
          onLayout={(e) => setScrollViewHeight(e.nativeEvent.layout.height)}
          refreshControl={
            <RefreshControl
              refreshing={false}
              enabled={isPullToRefreshEnabled}
              onRefresh={onRefresh}
              tintColor={COLORS.transparent}
              colors={[COLORS.transparent]}
              style={{backgroundColor: COLORS.transparent}}
            />
          }>
          <WebView
            source={{uri: url}}
            ref={webViewRef}
            style={WEBVIEW(scrollViewHeight)}
            onMessage={onWebViewMessage}
            injectedJavaScript={INJECTED_JS}
          />
        </ScrollView>
      ) : (
        <View style={styles.errorView}>
          <Text>Invalid url</Text>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  errorView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
