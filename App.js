/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View, StatusBar, Text} from 'react-native';
import {WebviewPullRefreshing} from './src/components/WebviewPullRefreshing';
import {helpers} from './src/utils/helpers';

const App = () => {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <WebviewPullRefreshing url="https://www.24h.com.vn/" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
